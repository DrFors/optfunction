
package mod2srmp.java;

import java.util.ArrayList;
import java.util.List;
import Parser.*;


public class RandomSearch implements Strategy {
    
    private double a;   
    private double b;
    private int n;
    private final List<Double> arrX = new ArrayList(); 
    private final List<Double> arrY = new ArrayList();
    private double resultY;
    private double resultX;
    private String funct;
    
      @Override
    public void setFunct(String f) {
       this.funct = f;
    }
    
    @Override
    public void setA(double a){this.a = a;}
 
    @Override
    public void setB(double b){this.b = b;}
    
    @Override
    public void setN(int n){this.n = n;}
    
    @Override
    public List getArrX(){return arrX;}
    
    @Override
    public List getArrY(){return arrY;}
    
    @Override
    public double getResultY(){return resultY;}
    
    @Override
    public double getResultX(){return resultX;}
    
    
    //double f(double x){
     //   return x*Math.exp(x)+Math.pow(x,2);
    //}
    

    
    double randX(){
        double x = a + Math.random()*(b-a);
        return x;
    }
    
    
    @Override
    public void result(){
        Parser p = new Parser();
        double x,f = 0,fmin = 100000;
        int iter = 0;
        
        for(int i = 0; i < n;i++){
        x = randX();
        p.setVariable("x", x);
        arrX.add(x);
        
            try {
                f = p.Parse(funct);
            } catch (Exception ex) {
               System.out.println("Err");
            }
        arrY.add(f);
        if(fmin > f){ fmin = f;
        iter = i;
        }}
        
        resultX = arrX.get(iter);
        resultY = fmin;
        
        
    }
    
}
